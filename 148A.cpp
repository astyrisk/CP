#include <bits/stdc++.h>
#include <cmath>

using namespace std;

int cnt = 0;

void solve() 
{ 
    int k, l, m, n, d;
    cin >> k >> l >> m >> n >> d;
    int arr[100001];
    for (int i = 1; i <= d; i++) arr[i] = 1;

    for (int i = 1; i <= d; i++) { 
        if (i%k == 0 || i % l == 0 || i % m == 0 || i % n == 0) arr[i] = 0;
    }

    for (int i = 1; i <= d; i++) {
        if (arr[i] == 0) cnt++;
    }

    cout << cnt << endl;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

