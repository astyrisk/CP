#include <bits/stdc++.h>

using namespace std;

int solve(int n) 
{ 
    if (n % 3 != 0) return n;
    return (solve(n/3));
}

int main() 
{ 
    int n, a, b, c;
    cin >> n;
    if ((n-2) % 3 != 0) {
        cout << 1 << " " << 1 << " " <<  n - 2 << endl;
    } else {
        cout << 1 << " " << 2 << " " << n -3 << endl;
    } 
}

