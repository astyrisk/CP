#include <bits/stdc++.h>

using namespace std;

int n;
int arr[101][101];
int sum = 0;

void solve() 
{ 
    cin >> n;
    int r = (n - 1) / 2;
    int cent = (n) / 2;

    for (int i = 0; i < n; i++) { 
        for (int j = 0; j < n; j++) { 
            cin >> arr[i][j];
        }
    }

    /*
    for (int i = 0; i < n; i++) { 
        cout << endl;
        for (int j = 0; j < n; j++) { 
            cout << arr[i][j] << "-" << i+1 << "," << j+1 << " ";
        }
    }
    */

    for (int i = 0; i < n; i++) { 
        //cout << arr[i][i];
        sum += arr[i][i];
    }

    for (int i = 0; i < n; i++){ 
        //cout << arr[i][n-i-1];
        sum += arr[i][n-i-1];
    }


    for (int i = 0; i < n; i++) { 
        //cout << arr[r][i]; 
        sum +=arr[r][i];
    }

    for (int i = 0; i < n; i++) {
        //cout << arr[i][r];
        sum +=arr[i][r];
    }

    sum -= 3 * arr[cent][cent];

    cout << sum << endl;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

