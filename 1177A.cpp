#include <bits/stdc++.h>
#include <string>

using namespace std;

void solve() 
{ 
    int n;
    cin >> n;
    string s, sum = "";
    for (int i = 1; i <= n; i++) {
        s = to_string(i);
        sum += s;
    }
    cout << sum[n-1] << endl;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

