#include <algorithm>
#include <bits/stdc++.h>
#include <cstdio>

using namespace std;

void sol()
{
    int n; 
    while(scanf("%d", &n) != EOF) {
        int x, a = 0, b = 0, c = 0;
        for (int i = 1; i <= n; i++) { 
            scanf("%d", &x);
            if (x == 1) a++;
            else if (x == 2) b++;
            else if (x == 3) c++;
        }
        int maxx = -1;
        maxx = max(a, b); 
        maxx = max(maxx, c); 
        cout << n - maxx << endl;
    }
}

void solve() 
{ 
    int a;
    int n; cin >> n;
    int cnt[3];
    for (int i = 0; i < n; i++) {
        cin >> a;
        if (a == 1) cnt[0]++;
        else if (a == 2) cnt[1]++;
        else if (a == 3) cnt[2]++;
    }

    int maxx = -1;
    maxx = max(cnt[0], cnt[1]);
    maxx = max(maxx, cnt[2]); 

    cout << n - maxx << endl;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

