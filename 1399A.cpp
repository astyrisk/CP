#include <bits/stdc++.h>

using namespace std;

void solve() 
{ 
    int n, arr[51];
    cin >> n;
    vector<int> a(n);
    for (auto &it: a) cin >> it;
    sort(a.begin(), a.end());
    bool ok = true;
    for (int i = 1; i < n; ++i) { 
        ok &= (a[i] - a[i - 1] <= 1);
    }

    if (ok) cout << "YES" << endl;
    else cout << "NO" << endl;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

