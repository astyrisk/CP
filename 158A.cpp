#include <bits/stdc++.h>

using namespace std;

void solve() 
{ 
    int n, k, arr[101], cnt = 0;
    cin >> n >> k;
    for (int i = 0; i < n; i++) { 
        cin >> arr[i];
    }
    for (int i = 0; i < n; i++) { 
        if (arr[i] > 0 && arr[i] >= arr[k-1]) 
            cnt++;
    }
    cout << cnt;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

