#include <bits/stdc++.h>

using namespace std;
bool isPrime(int n)
{
    if (n <= 1) return false;
    for (int i = 2; i < n; i++)
        if (n % i == 0) return false;
    return true;
}

int sol(int a, int n)
{
    if (isPrime(n) || isPrime(n - a)) return sol(a, n*2);
    return n;
}

void solve() 
{ 
    int n;
    cin >> n;
    int a = sol(n, n*2);
    cout << a << " " << a - n << endl;

}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

