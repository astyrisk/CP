#include <bits/stdc++.h>

using namespace std;


void solve() 
{ 
    int n; 
    cin >> n;
    //cout << "n: " << n << endl;
    long long sum = 1ll * (pow(n,2) + n)/2;
    //cout << "sum: " << sum << endl;
    if ((sum % 2)  == 0) cout << 0 << endl;
    else cout << 1 << endl;

}

void sol()
{
    int n;
    cin >> n;
    long long sum = n * 1ll * (n+1) / 2;

}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve();
    }
}

