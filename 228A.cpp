#include <bits/stdc++.h>
#include <cstring>
#include <string>

using namespace std;


void solve() 
{
    char ch;
    char arr[101];
    int k = 0, s = 0;
    while (cin >> ch) {
        int exist = 0;
        for (int i = 0; i < k; i++) {
            if (arr[i] == ch) { 
                exist = 1;
                break;
            }
        }
        arr[k++] = ch;
        if (!exist) s++;
    }
    if (s % 2 == 0) cout << "CHAT WITH HER!";
    else cout << "IGNORE HIM!";
}

int main() 
{
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        // cout << "Case #" << t << ": ";
        solve();
    }
    return 0;
}

