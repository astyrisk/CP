#include <bits/stdc++.h>

using namespace std;

void solve() { 
    int sum = 0;
    int n; cin >> n;
    while  (n--) { 
        int p, l = 0;
        for (int i = 0; i <= 2; i++) { 
            cin >> p;
            if (p) l++;
        }
        if (l >= 2) sum++;
    }
    cout << sum << endl;
}

int main() { 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        // cout << "Case #" << t << ": ";
        solve();
    }
}

