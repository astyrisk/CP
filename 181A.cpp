#include <bits/stdc++.h>

using namespace std;

char ch;

struct point { 
    int x;
    int y;
};

struct point asts[4];

void solve() 
{ 
    int n, m, cnt = 0, rowcnt = 0, astcnt = 0;

    cin >> n >> m;

    for (int j = 0; j < n; j++) 
        for (int i = 0; i < m; i++) { 
            cin >> ch; 
            if (ch == '*') { 
                asts[astcnt].y = i + 1;
                asts[astcnt++].x = j + 1;
            }
        }

    for (int i = 0; i < 3; i++, rowcnt = 0) 
        for (int j = 0; j < 3; j++) {
            if (asts[i].x == asts[j].x) rowcnt++;
            if ((j == 2) && rowcnt == 1) asts[3].x = asts[i].x;
        }
    
    for (int i = 0; i < 3; i++, rowcnt = 0) 
        for (int j = 0; j < 3; j++) {
            if (asts[i].y == asts[j].y) rowcnt++;
            if ((j == 2) && rowcnt == 1) asts[3].y = asts[i].y;
        }

    cout << asts[3].x << " " << asts[3].y;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        // cout << "Case #" << t << ": ";
        solve();
    }
}

