#include <bits/stdc++.h>

using namespace std;

bool luck = true;
int sum1 = 0, sum2 = 0;

void solve2()
{
    int n;
    string s;
    cin >> n >> s;

    int diff(0);
    n /= 2;
    
    for (size_t i = 0; i < n; i++) { 
        if ((s[i] != '4' && s[i] != '7') || (s[n+i] != '4' && s[n+i] != '7')) {
            luck = false;
            break;
        }
        diff += (s[i] - s[n+i]);
    }
    luck = luck && (diff == 0);
    cout << (luck ? "YES" : "NO") << endl;
}
void solve()  // up to 20 digits
{ 
    int n, arr[101]; 
    unsigned long long d;
    cin >> n >> d;
    cout << d << endl;

    for (int i = n-1; i >= 0; i--) {
        arr[i] = d % 10;
        d /= 10;
    }

    cout << "array: ";
    for (int i = 0; i < n; i++) cout << arr[i] << " ";
    cout << endl;

    for (int i = 0; i < n; i++) {
        if (arr[i] != 7 && arr[i] != 4) {
            luck = false;
            break;
        }
    }

    for (int i = 0; i < n/2; i++) {
        sum1 += arr[i];
    }
    for (int i = n/2; i < n; i++) {
        sum2 += arr[i];
    }

    if (sum1 == sum2 && luck) cout << "YES";
    else cout << "NO";
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        solve2();
    }
}

