#include <bits/stdc++.h> 

using namespace std;

void solve() 
{ 
    int n, m, cnt = 0;
    cin >> n >> m;

    for (int i = 0; i < 1000; i++) {
        for (int j = 0; j < 1000; j++) { 
            if (((i * i) + j == n)  && (i + (j * j) == m)) cnt++;
        }
    }
    cout << cnt << endl;
}

int main() 
{ 
    ios_base::sync_with_stdio(0);
    cin.tie(0); cout.tie(0);
    int tc = 1;
    // cin >> tc;
    for (int t = 1; t <= tc; t++) {
        // cout << "Case #" << t << ": ";
        solve();
    }
}
